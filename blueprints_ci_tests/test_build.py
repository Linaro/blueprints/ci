from unittest import TestCase
from unittest.mock import Mock, patch
from blueprints_ci.build import build
import shutil

import pathlib


class TestBuild(TestCase):

    def sample_settings(self):
        return Mock(
            CI_COMMIT_SHORT_SHA="123abc",
            CI_JOB_NAME="sample-job",
            CI_JOB_ID="123",
            CI_PIPELINE_SOURCE="push",
            CI_PROJECT_DIR=".",
            CI_PROJECT_PATH="this/project",
            DEVICE="qemu",
            IMAGE="fake_image",
            IMAGES_DIR="images",
            IS_MERGE_REQUEST=False,
            SQUAD_HOST="http://squad.example",
            SQUAD_TOKEN="123",
            SQUAD_GROUP="squad_group",
            SQUAD_PROJECT="squad_project",
            TUXSUITE_TOKEN_MR="secret-token",
            RUNNING_NIGHTLY=None,
            ARTIFACTS=None,
            ARTIFACTS_DIR="artifacts",
            get_artifacts=dict,
        )

    @patch("blueprints_ci.build.submit_to_tuxsuite")
    @patch("blueprints_ci.build.download_image")
    def test_build_os(self, mock_download_image, mock_submit_to_tuxsuite):
        results_file = pathlib.Path("result.json")
        results_file.touch()
        mock_submit_to_tuxsuite.return_value = (True, results_file)

        image_contents = "image contents"
        mock_image = pathlib.Path("fake_image.gz")
        mock_image.write_text(image_contents)
        mock_download_image.return_value = (True, mock_image)

        settings = self.sample_settings()
        final_image = pathlib.Path(settings.IMAGES_DIR) / mock_image.name
        build_env = pathlib.Path(settings.CI_PROJECT_DIR) / "build.env"

        self.assertTrue(build(settings))

        mock_submit_to_tuxsuite.assert_called()
        mock_download_image.assert_called()

        self.assertEqual(final_image.read_text(), image_contents)

        build_env_contents = build_env.read_text()
        self.assertIn(f"SQUAD_HOST={settings.SQUAD_HOST}", build_env_contents)
        self.assertIn(f"SQUAD_TOKEN={settings.SQUAD_TOKEN}", build_env_contents)
        self.assertIn(f"SQUAD_GROUP={settings.SQUAD_GROUP}", build_env_contents)
        self.assertIn(f"SQUAD_PROJECT={settings.SQUAD_PROJECT}", build_env_contents)
        self.assertIn(f"SQUAD_BUILD={settings.CI_COMMIT_SHORT_SHA}", build_env_contents)
        self.assertIn(f"OS_BUILD_JOB_ID={settings.CI_JOB_ID}", build_env_contents)
        self.assertIn(f"OS_RESULT_IMAGE_FILE={mock_image.name}", build_env_contents)

        final_image.unlink()
        results_file.unlink()
        build_env.unlink()
        shutil.rmtree(settings.IMAGES_DIR)

    @patch("blueprints_ci.build.submit_to_tuxsuite")
    @patch("blueprints_ci.build.download_image")
    def test_build_firmware(self, mock_download_image, mock_submit_to_tuxsuite):
        results_file = pathlib.Path("result.json")
        results_file.touch()
        mock_submit_to_tuxsuite.return_value = (True, results_file)

        image_contents = "image contents"
        mock_image = pathlib.Path("fake_image.gz")
        mock_image.write_text(image_contents)
        mock_download_image.return_value = (True, mock_image)

        settings = self.sample_settings()
        settings.CI_JOB_NAME = "build-meta-ts"
        final_image = pathlib.Path(settings.IMAGES_DIR) / mock_image.name
        build_env = pathlib.Path(settings.CI_PROJECT_DIR) / "build.env"

        self.assertTrue(build(settings))

        mock_submit_to_tuxsuite.assert_called()
        mock_download_image.assert_called()

        self.assertEqual(final_image.read_text(), image_contents)

        build_env_contents = build_env.read_text()
        self.assertIn(f"SQUAD_HOST={settings.SQUAD_HOST}", build_env_contents)
        self.assertIn(f"SQUAD_TOKEN={settings.SQUAD_TOKEN}", build_env_contents)
        self.assertIn(f"SQUAD_GROUP={settings.SQUAD_GROUP}", build_env_contents)
        self.assertIn(f"SQUAD_PROJECT={settings.SQUAD_PROJECT}", build_env_contents)
        self.assertIn(f"SQUAD_BUILD={settings.CI_COMMIT_SHORT_SHA}", build_env_contents)
        self.assertIn(f"FIRMWARE_BUILD_JOB_ID={settings.CI_JOB_ID}", build_env_contents)
        self.assertIn(f"FIRMWARE_RESULT_IMAGE_FILE={mock_image.name}", build_env_contents)

        final_image.unlink()
        results_file.unlink()
        build_env.unlink()
        shutil.rmtree(settings.IMAGES_DIR)
