import os

from unittest import TestCase
from unittest.mock import Mock
from blueprints_ci.kas_local_config import kas_local_config


class TestKasLocalConfig(TestCase):

    def sample_settings(self):
        return Mock(
            CI_PROJECT_NAME="meta-ewaol",
            CI_MERGE_REQUEST_IID="123",
            IS_MERGE_REQUEST=True,
        )

    def test_run_only_on_meta_ewaol_merge_requests(self):
        settings = self.sample_settings()
        settings.IS_MERGE_REQUEST = False
        self.assertFalse(kas_local_config(settings))

        settings.IS_MERGE_REQUEST = True
        settings.CI_PROJECT_NAME = "meta-ewaol-machine"
        self.assertFalse(kas_local_config(settings))

    def test_run_only_if_merge_request_internal_id_is_present(self):
        settings = self.sample_settings()
        settings.CI_MERGE_REQUEST_IID = None
        self.assertFalse(kas_local_config(settings))

    def test_generate_kas_local_config(self):
        settings = self.sample_settings()
        self.assertTrue(kas_local_config(settings))
        filename = "kas-local-config.yml"
        self.assertTrue(os.path.exists(filename))
        with open(filename, "r") as fp:
            self.assertEqual(fp.read(), f"""header:
  version: 14

repos:
  meta-ewaol:
    url: https://gitlab.com/soafee/ewaol/meta-ewaol.git
    refspec: refs/merge-requests/{settings.CI_MERGE_REQUEST_IID}/merge
    path: layers/meta-ewaol
""")
        os.unlink(filename)
