import os

from unittest import TestCase
from unittest.mock import Mock, patch
from blueprints_ci.check import check
from squad_client.core import models


class TestCheck(TestCase):

    def sample_settings(self):
        return Mock(
            SQUAD_JOB_ID="123",
            SQUAD_HOST="https://squad.example",
        )

    def sample_testjob(self):
        testjob = models.TestJob()
        testjob.external_url = "https://lkft.validation.linaro.org/scheduler/job/5984032"
        testjob.environment = "qemu"
        testjob.created_at = "2022-12-22T14:57:22.392137Z"
        testjob.fetched_at = "2022-12-22T17:02:02.081124Z"
        testjob.started_at = "2022-12-22T16:52:21Z"
        testjob.ended_at = "2022-12-22T17:01:59Z"
        testjob.job_status = "Complete"
        testjob.log = "Test job log"
        testjob.failure = None
        testjob.fetched = True

        test1 = models.Test()
        test1.name = "suite/test1"
        test1.status = "pass"

        test2 = models.Test()
        test2.name = "suite/test2"
        test2.status = "pass"

        testjob.tests = [test1, test2]

        return testjob

    @patch("blueprints_ci.check.fetch_job_from_squad")
    def test_basics(self, fetch_job_from_squad_mock):
        testjob = self.sample_testjob()
        fetch_job_from_squad_mock.return_value = testjob
        settings = self.sample_settings()
        self.assertTrue(check(settings))

    @patch("blueprints_ci.check.fetch_job_from_squad")
    def test_fail_job_not_ready_yet(self, fetch_job_from_squad_mock):
        testjob = self.sample_testjob()
        testjob.fetched = False
        fetch_job_from_squad_mock.return_value = testjob
        settings = self.sample_settings()
        self.assertFalse(check(settings))

    @patch("blueprints_ci.check.get_test_plan")
    @patch("blueprints_ci.check.fetch_job_from_squad")
    def test_fail_if_job_didnot_finish_correctly(self, fetch_job_from_squad_mock, get_test_plan_mock):
        settings = self.sample_settings()
        settings.LAVA_DEVICE = "qemu"
        testjob = self.sample_testjob()
        testjob.job_status = "Incomplete"
        fetch_job_from_squad_mock.return_value = testjob
        get_test_plan_mock.return_value = {settings.LAVA_DEVICE: {}}
        self.assertFalse(check(settings))

    @patch("blueprints_ci.check.fetch_job_from_squad")
    def test_fail_if_at_least_one_failed_test(self, fetch_job_from_squad_mock):
        bad_test = models.Test()
        bad_test.name = "suite/test3"
        bad_test.status = "fail"

        testjob = self.sample_testjob()
        testjob.tests.append(bad_test)
        fetch_job_from_squad_mock.return_value = testjob
        settings = self.sample_settings()
        self.assertFalse(check(settings))

    @patch("blueprints_ci.check.fetch_job_from_squad")
    def test_fail_if_no_tests(self, fetch_job_from_squad_mock):
        testjob = self.sample_testjob()
        testjob.tests = []
        fetch_job_from_squad_mock.return_value = testjob
        settings = self.sample_settings()
        self.assertFalse(check(settings))

    @patch("blueprints_ci.download_file")
    @patch("blueprints_ci.SquadApi")
    @patch("blueprints_ci.TestRun")
    @patch("blueprints_ci.TestJob")
    def test_replace_failed_job_for_successful_resubmission(self, mock_testjob, mock_testrun, mock_squadapi, mock_download_file):
        """
            By default, SQUAD resubmits jobs that failed due to occasional infrastructure errors.
            Such resubmission can return successful jobs.
        """
        resubmitted_job = self.sample_testjob()
        resubmitted_job.testrun = "testrun/1/"

        testjob = self.sample_testjob()
        testjob.job_status = "Incomplete"
        testjob.can_resubmit = True
        testjob.failure = "Sample failure"
        testjob.resubmitted_jobs = lambda: {1: resubmitted_job}
        mock_testjob.return_value = testjob

        test = models.Test()
        test.name = "suite/test"
        test.status = "pass"
        testrun = models.TestRun()
        testrun.add_test(test)
        testrun.log_file = "log_file"
        mock_testrun.return_value = testrun

        settings = self.sample_settings()
        job_log_filename = f"job-{settings.SQUAD_JOB_ID}.log"
        with open(job_log_filename, "w") as fp:
            fp.write("Sample jog log")

        self.assertTrue(check(settings))
        os.unlink(job_log_filename)

    @patch("blueprints_ci.check.get_test_plan")
    @patch("blueprints_ci.check.fetch_job_from_squad")
    def test_bypass_failure(self, fetch_job_from_squad_mock, get_test_plan_mock):
        settings = self.sample_settings()
        settings.LAVA_DEVICE = "qemu"
        testjob = self.sample_testjob()
        testjob.job_status = "Incomplete"
        fetch_job_from_squad_mock.return_value = testjob
        get_test_plan_mock.return_value = {settings.LAVA_DEVICE: {"bypass_failure": True}}
        self.assertEqual(None, check(settings))
