from unittest import TestCase
from unittest.mock import Mock, patch
from blueprints_ci.test import test

import pathlib


class TestTest(TestCase):

    def sample_settings(self):
        return Mock(
            CI_JOB_NAME="sample-job",
            CI_JOB_ID="123",
            CI_PROJECT_DIR=".",
            CI_PROJECT_PATH="this/project",
            CI_PROJECT_URL="http://this.project",
            LAVA_DEVICE="qemu",
            SQUAD_HOST="http://squad.example",
            SQUAD_TOKEN="123",
            SQUAD_GROUP="squad_group",
            SQUAD_PROJECT="squad_project",
            SQUAD_BUILD="123abc",
            IMAGES_DIR="images",
            BUILD_JOB_ID="1234",
            SKIP=None,
            TESTS=None,
            RUNNING_NIGHTLY=None,
            OS=None,
            OS_BUILD_JOB_ID=None,
            OS_RESULT_IMAGE_FILE=None,
            OS_ARTIFACTS=None,
            FIRMWARE=None,
            FIRMWARE_BUILD_JOB_ID=None,
            FIRMWARE_ARTIFACTS=None,
            FIRMWARE_RESULT_IMAGE_FILE=None,
            get_artifacts=dict,
        )

    @patch("blueprints_ci.test.register_callback_in_squad")
    @patch("blueprints_ci.test.send_testjob_request_to_squad")
    def test_os_test(self, mock_send_testjob_request_to_squad, mock_register_callback_in_squad):
        mock_send_testjob_request_to_squad.return_value = (True, "1")
        mock_register_callback_in_squad.return_value = True
        settings = self.sample_settings()
        settings.OS_BUILD_JOB_ID = "12345"
        settings.OS_RESULT_IMAGE_FILE = "os.gz"
        settings.FIRMWARE = "http://firmware.url"
        self.assertTrue(test(settings))

        test_env = pathlib.Path(settings.CI_PROJECT_DIR) / "test.env"
        test_env_contents = test_env.read_text()
        self.assertIn("SQUAD_JOB_ID=1", test_env_contents)
        self.assertIn(f"SQUAD_GROUP={settings.SQUAD_GROUP}", test_env_contents)
        self.assertIn(f"SQUAD_PROJECT={settings.SQUAD_PROJECT}", test_env_contents)
        test_env.unlink()

    @patch("blueprints_ci.test.register_callback_in_squad")
    @patch("blueprints_ci.test.send_testjob_request_to_squad")
    def test_firmware_test(self, mock_send_testjob_request_to_squad, mock_register_callback_in_squad):
        mock_send_testjob_request_to_squad.return_value = (True, "1")
        mock_register_callback_in_squad.return_value = True
        settings = self.sample_settings()
        settings.FIRMWARE_BUILD_JOB_ID = "12345"
        settings.FIRMWARE_RESULT_IMAGE_FILE = "firmware.gz"
        settings.OS = "http://firmware.url"
        self.assertTrue(test(settings))

        test_env = pathlib.Path(settings.CI_PROJECT_DIR) / "test.env"
        test_env_contents = test_env.read_text()
        self.assertIn("SQUAD_JOB_ID=1", test_env_contents)
        self.assertIn(f"SQUAD_GROUP={settings.SQUAD_GROUP}", test_env_contents)
        self.assertIn(f"SQUAD_PROJECT={settings.SQUAD_PROJECT}", test_env_contents)
        test_env.unlink()

    @patch("blueprints_ci.test.register_callback_in_squad")
    @patch("blueprints_ci.test.send_testjob_request_to_squad")
    def test_fail_to_register_callback(self, mock_send_testjob_request_to_squad, mock_register_callback_in_squad):
        mock_send_testjob_request_to_squad.return_value = (True, "1")
        mock_register_callback_in_squad.return_value = False
        settings = self.sample_settings()
        settings.FIRMWARE_BUILD_JOB_ID = "12345"
        settings.FIRMWARE_RESULT_IMAGE_FILE = "firmware.gz"
        settings.OS = "http://firmware.url"
        self.assertTrue(test(settings))

        test_env = pathlib.Path(settings.CI_PROJECT_DIR) / "test.env"
        test_env.unlink()
