import os
import shutil
import xml.etree.ElementTree as ET

from datetime import datetime
from unittest import TestCase
from unittest.mock import call, patch, Mock, MagicMock
from blueprints_ci.tip import (
    tip,
    check_project_changes,
    build_individual_project,
    create_manifest_mr,
)


DEFAULT_XML_FILENAME = "default.xml"
ROOT = os.getcwd()


class LocalSettings():
    def get_artifacts(self):
        return {}


class TestTip(TestCase):

    def sample_settings(self):
        settings = LocalSettings()
        settings.GIT_URL = "http://some.git/url"
        settings.GIT_BRANCH = "some-branch"
        settings.CI_COMMIT_TAG = None
        settings.CI_JOB_NAME = "test-tip-job-name"
        settings.CI_PROJECT_DIR = "."
        settings.CI_PROJECT_PATH = "Linaro/trusted-reference-stack/trs-manifest"
        settings.IS_TRS_MANIFEST_REPO = True
        settings.TEMPLATECONF = "../trs/meta-trs/conf/templates/"
        settings.EXTRACONFIGS = "../meta-ts/meta-trustedsubstrate/conf/templates/multiconfig/,../meta-trs/conf/templates/multiconfig"
        settings.TARGET_TYPE = "trs-image"
        settings.TARGET = "trs-qemuarm64"
        settings.ARTIFACTS = None
        settings.TRS_MANIFEST_URL = "https://gitlab.com/Linaro/trs/trs-manifest/"
        settings.PRINT_BUILD_LOG = "false"
        settings.TUXSUITE_DOWNLOAD_URL = "https://tuxsuite.com/builds/123"

        return settings

    def setUp(self):
        with open(DEFAULT_XML_FILENAME, "w") as fp:
            fp.write("""<?xml version="1.0" encoding="UTF-8"?>
                        <manifest>
                            <remote name="gitlab" fetch="https://gitlab.com" />
                            <default remote="gitlab" revision="main" />
                            <project path="with-new-revisions" name="with/new/revisions.git" revision="1" upstream="refs/heads/main" />
                            <project path="no-new-revisions" name="no/new/revisions.git" revision="1" upstream="refs/heads/main" />
                            <project path="breaking-new-revisions" name="breaking/new/revisions.git" revision="1" upstream="refs/heads/main" />
                        </manifest>""")

    def tearDown(self):
        if os.path.exists(DEFAULT_XML_FILENAME):
            os.unlink(DEFAULT_XML_FILENAME)

        # Make sure to always be in project's root folder
        os.chdir(ROOT)

    def __create_manifest_mr(settings, good_revisions, default_xml):
        return False

    def __download_file(url, output_filename=None):
        shutil.copy(os.path.join("..", DEFAULT_XML_FILENAME), DEFAULT_XML_FILENAME)
        return True

    def __run_cmd(cmd):
        proc = Mock()
        proc.ok = True
        proc.out = ""
        proc.err = ""
        git_cmd = cmd[1]
        if git_cmd == "ls-remote":
            repo = cmd[2]
            if "bad-remote" in repo:
                proc.ok = False
                proc.err = "bad remote"

            elif "noref-remote" in repo:
                proc.out = "1 refs/does/not/exist"

            elif "no/new/revisions" in repo:
                proc.out = "1 refs/heads/main"

            elif "with/new/revisions" in repo:
                proc.out = "2 refs/heads/main"

            elif "breaking/new/revisions" in repo:
                proc.out = "3 refs/heads/main"

        return proc

    def __submit_to_tuxsuite(settings, plan):
        if "breaking-new-revisions" in plan["name"]:
            return False
        return True

    def test_not_on_trs_manifest_repo(self):
        settings = self.sample_settings()
        settings.IS_TRS_MANIFEST_REPO = None
        self.assertFalse(tip(settings))

    def test_manifest_not_present(self):
        settings = self.sample_settings()
        os.unlink(DEFAULT_XML_FILENAME)
        self.assertFalse(tip(settings))

    @patch("blueprints_ci.tip.run_cmd", side_effect=Exception())
    def test_bad_git(self, mock_run_cmd):
        # When running git is bad, projects will simply be ignored
        settings = self.sample_settings()
        self.assertTrue(tip(settings))

    @patch("blueprints_ci.tip.run_cmd", side_effect=__run_cmd)
    def test_check_project_changes_unresponsive_remote(self, mock_run_cmd):
        project = ET.Element("project", attrib={
            "revision": "1",
            "upstream": "refs/heads/main",
            "name": "with/new/revisions.git",
            "remote": "bad-remote",
        })

        bad_remote = ET.Element("remote", attrib={"fetch": "https://bad-remote.com"})
        remotes = {
            "bad-remote": bad_remote,
        }

        self.assertEqual((project, None), check_project_changes((project, remotes)))

    @patch("blueprints_ci.tip.run_cmd", side_effect=__run_cmd)
    def test_check_project_changes_noref_remote(self, mock_run_cmd):
        project = ET.Element("project", attrib={
            "revision": "1",
            "upstream": "refs/heads/main",
            "name": "with/new/revisions.git",
            "remote": "noref-remote",
        })

        noref_remote = ET.Element("remote", attrib={"fetch": "https://noref-remote.com"})
        remotes = {
            "noref-remote": noref_remote,
        }

        self.assertEqual((project, None), check_project_changes((project, remotes)))

    @patch("blueprints_ci.tip.run_cmd", side_effect=__run_cmd)
    def test_check_project_changes_no_new_revisions(self, mock_run_cmd):
        project = ET.Element("project", attrib={
            "revision": "1",
            "upstream": "refs/heads/main",
            "name": "no/new/revisions.git",
            "remote": "good-remote"
        })

        good_remote = ET.Element("remote", attrib={"fetch": "https://good-remote.com"})
        remotes = {
            "good-remote": good_remote,
        }

        self.assertEqual((project, False), check_project_changes((project, remotes)))

    @patch("blueprints_ci.tip.run_cmd", side_effect=__run_cmd)
    def test_check_project_changes_no_upstream(self, mock_run_cmd):
        project = ET.Element("project", attrib={
            "revision": "1",
            "name": "no/upstream.git",
            "remote": "good-remote"
        })

        good_remote = ET.Element("remote", attrib={"fetch": "https://good-remote.com"})
        remotes = {
            "good-remote": good_remote,
        }

        self.assertEqual((project, False), check_project_changes((project, remotes)))
        mock_run_cmd.assert_not_called()

    @patch("blueprints_ci.tip.run_cmd", side_effect=__run_cmd)
    def test_check_project_changes_new_revisions(self, mock_run_cmd):
        project = ET.Element("project", attrib={
            "revision": "1",
            "upstream": "refs/heads/main",
            "name": "with/new/revisions.git",
            "remote": "good-remote"
        })

        good_remote = ET.Element("remote", attrib={"fetch": "https://good-remote.com"})
        remotes = {
            "good-remote": good_remote,
        }

        new_revision = "2"
        self.assertEqual((project, new_revision), check_project_changes((project, remotes)))

    @patch("blueprints_ci.tip.run_cmd", side_effect=__run_cmd)
    def test_no_new_revisions(self, mock_run_cmd):
        with open(DEFAULT_XML_FILENAME, "w") as fp:
            fp.write("""<?xml version="1.0" encoding="UTF-8"?>
                        <manifest>
                            <remote name="gitlab" fetch="https://gitlab.com" />
                            <default remote="gitlab" revision="main" />
                            <project path="no-new-revisions" name="no/new/revisions.git" revision="1" upstream="refs/heads/main" />
                        </manifest>""")

        settings = self.sample_settings()
        self.assertTrue(tip(settings))

    @patch("blueprints_ci.download_file", return_value=True)
    @patch("blueprints_ci.tip.submit_to_tuxsuite", side_effect=__submit_to_tuxsuite)
    def test_build_individual_project_failed(self, mock_submit_to_tuxsuite, mock_download_file):
        settings = self.sample_settings()
        project = ET.Element("project", attrib={
            "revision": "1",
            "name": "breaking/new/revisions.git",
            "path": "breaking-new-revisions",
            "upstream": "refs/heads/main",
        })

        new_revision = "2"
        build_folder = project.attrib["name"].replace("/", "-").replace(".git", "")
        os.makedirs(build_folder, exist_ok=True)
        shutil.copy(DEFAULT_XML_FILENAME, os.path.join(build_folder, DEFAULT_XML_FILENAME))
        settings.CI_JOB_NAME = project.attrib["path"]
        self.assertEqual((project, new_revision, settings.TUXSUITE_DOWNLOAD_URL, False), build_individual_project((settings, project, new_revision)))
        mock_download_file.assert_called()

        with open(os.path.join(build_folder, "local-manifest.xml"), "r") as fp:
            local_manifest_xml_contents = fp.read()
            self.assertIn(f"<remove-project name=\"{project.attrib['name']}\" />", local_manifest_xml_contents)
            self.assertIn(f"<project path=\"{project.attrib['path']}\" name=\"{project.attrib['name']}\" revision=\"2\" upstream=\"{project.attrib['upstream']}\" />", local_manifest_xml_contents)

        shutil.rmtree(build_folder)

    @patch("blueprints_ci.download_file", return_value=True)
    @patch("blueprints_ci.tip.submit_to_tuxsuite", side_effect=__submit_to_tuxsuite)
    def test_build_individual_project_successful(self, mock_submit_to_tuxsuite, mock_download_file):
        settings = self.sample_settings()
        project = ET.Element("project", attrib={
            "revision": "1",
            "name": "with/new/revisions.git",
            "path": "with-new-revisions",
            "upstream": "refs/heads/main",
        })

        new_revision = "2"
        build_folder = project.attrib["name"].replace("/", "-").replace(".git", "")
        os.makedirs(build_folder, exist_ok=True)
        shutil.copy(DEFAULT_XML_FILENAME, os.path.join(build_folder, DEFAULT_XML_FILENAME))
        settings.CI_JOB_NAME = project.attrib["path"]
        self.assertEqual((project, new_revision, settings.TUXSUITE_DOWNLOAD_URL, True), build_individual_project((settings, project, new_revision)))
        mock_download_file.assert_called()

        with open(os.path.join(build_folder, "local-manifest.xml"), "r") as fp:
            local_manifest_xml_contents = fp.read()
            self.assertIn(f"<remove-project name=\"{project.attrib['name']}\" />", local_manifest_xml_contents)
            self.assertIn(f"<project path=\"{project.attrib['path']}\" name=\"{project.attrib['name']}\" revision=\"2\" upstream=\"{project.attrib['upstream']}\" />", local_manifest_xml_contents)

        shutil.rmtree(build_folder)

    @patch("blueprints_ci.download_file", side_effect=__download_file)
    @patch("blueprints_ci.tip.create_manifest_mr")
    @patch("blueprints_ci.tip.submit_to_tuxsuite", side_effect=__submit_to_tuxsuite)
    @patch("blueprints_ci.tip.run_cmd", side_effect=__run_cmd)
    def test_no_changes_detected(self, mock_run_cmd, mock_submit_to_tuxsuite, mock_create_manifest_mr, mock_download_file):
        settings = self.sample_settings()
        with open(DEFAULT_XML_FILENAME, "w") as fp:
            fp.write("""<?xml version="1.0" encoding="UTF-8"?>
                        <manifest>
                            <remote name="gitlab" fetch="https://gitlab.com" />
                            <default remote="gitlab" revision="main" />
                            <project path="no-new-revisions" name="no/new/revisions.git" revision="1" upstream="refs/heads/main" />
                        </manifest>""")
        settings.CI_JOB_NAME = "no-new-revisions"
        self.assertTrue(tip(settings))
        mock_submit_to_tuxsuite.assert_not_called()
        mock_download_file.assert_not_called()
        mock_create_manifest_mr.assert_not_called()

    @patch("blueprints_ci.download_file", side_effect=__download_file)
    @patch("blueprints_ci.tip.create_manifest_mr")
    @patch("blueprints_ci.tip.submit_to_tuxsuite", side_effect=__submit_to_tuxsuite)
    @patch("blueprints_ci.tip.run_cmd", side_effect=__run_cmd)
    def test_no_good_builds(self, mock_run_cmd, mock_submit_to_tuxsuite, mock_create_manifest_mr, mock_download_file):
        settings = self.sample_settings()
        with open(DEFAULT_XML_FILENAME, "w") as fp:
            fp.write("""<?xml version="1.0" encoding="UTF-8"?>
                        <manifest>
                            <remote name="gitlab" fetch="https://gitlab.com" />
                            <default remote="gitlab" revision="main" />
                            <project path="breaking-new-revisions" name="breaking/new/revisions.git" revision="1" upstream="refs/heads/main" />
                        </manifest>""")
        settings.CI_JOB_NAME = "breaking-new-revisions"
        self.assertTrue(tip(settings))
        # FIXME: mocks with `side_effect` do not keep of the number of times they were called.
        # therefore, the check below doesn't work. This is a Python "feature": https://bugs.python.org/issue38763
        # mock_submit_to_tuxsuite.assert_called()
        # mock_download_file.assert_called()
        mock_create_manifest_mr.assert_not_called()
        shutil.rmtree("breaking-new-revisions")

    @patch("blueprints_ci.download_file", side_effect=__download_file)
    @patch("blueprints_ci.tip.create_manifest_mr")
    @patch("blueprints_ci.tip.submit_to_tuxsuite", side_effect=__submit_to_tuxsuite)
    @patch("blueprints_ci.tip.run_cmd", side_effect=__run_cmd)
    def test_create_mr_only_projects_with_new_revisions_and_successful_builds(self, mock_run_cmd, mock_submit_to_tuxsuite, mock_create_manifest_mr, mock_download_file):
        settings = self.sample_settings()
        self.assertTrue(tip(settings))
        mock_create_manifest_mr.assert_called()
        shutil.rmtree("breaking-new-revisions")
        shutil.rmtree("with-new-revisions")


class TestCreateMr(TestCase):

    def sample_settings(self):
        settings = LocalSettings()
        settings.ARTIFACTS = None
        settings.TRS_MANIFEST_MR_ASSIGNEE_IDS = None
        settings.CI_JOB_ID = "123"
        settings.CI_PROJECT_ID = "1"
        settings.CI_PROJECT_URL = "https://gitlab.com/Linaro/trusted-reference-stack/trs-manifest"
        settings.CI_PROJECT_PATH = "Linaro/trusted-reference-stack/trs-manifest"
        settings.CI_SERVER_HOST = "gitlab.com"
        settings.CI_API_V4_URL = "https://gitlab.com/api/v4"
        settings.TRS_MANIFEST_BOT_USER_TOKEN = "12345"
        settings.TRS_MANIFEST_BOT_USER_NAME = "BlueprintsCI bot"
        settings.TUXSUITE_DOWNLOAD_URL = "https://tuxsuite.com/builds/123"
        return settings

    def setUp(self):
        with open(DEFAULT_XML_FILENAME, "w") as fp:
            fp.write("""<?xml version="1.0" encoding="UTF-8"?>
                        <manifest>
                            <remote name="gitlab" fetch="https://gitlab.com" />
                            <default remote="gitlab" revision="main" />
                            <project path="with-new-revisions" name="with/new/revisions.git" revision="1" upstream="refs/heads/main" />
                            <project path="no-new-revisions" name="no/new/revisions.git" revision="1" upstream="refs/heads/main" />
                            <project path="breaking-new-revisions" name="breaking/new/revisions.git" revision="1" upstream="refs/heads/main" />
                        </manifest>""")

    def tearDown(self):
        if os.path.exists(DEFAULT_XML_FILENAME):
            os.unlink(DEFAULT_XML_FILENAME)

    def test_no_good_revisions(self):
        self.assertFalse(create_manifest_mr(None, [], [], None))

    @patch("blueprints_ci.tip.run_git", side_effect=Exception("bad git"))
    def test_failed_to_run_git(self, mock_run_git):
        new_revision = "5"

        settings = self.sample_settings()

        default_xml = ET.parse(DEFAULT_XML_FILENAME)
        root = default_xml.getroot()
        project = root.findall("project")[0]
        good_revisions = [(project, new_revision)]

        self.assertFalse(create_manifest_mr(settings, good_revisions, [], default_xml))

    @patch("blueprints_ci.tip.requests")
    @patch("blueprints_ci.tip.run_git")
    def test_failed_to_post(self, mock_run_git, mock_requests):
        new_revision = "5"
        assignees = ["1", "2"]

        settings = self.sample_settings()
        settings.TRS_MANIFEST_MR_ASSIGNEE_IDS = ",".join(assignees)

        expected_branch_name = "track-master-" + datetime.now().strftime("%d-%m-%Y")
        git_user = f"project_{settings.CI_PROJECT_ID}_bot"
        git_url = f"https://{git_user}:{settings.TRS_MANIFEST_BOT_USER_TOKEN}@{settings.CI_SERVER_HOST}/{settings.CI_PROJECT_PATH}.git"
        config_user = [
            "-c", f"user.email='{git_user}@{settings.CI_SERVER_HOST}'",
            "-c", f"user.name='{settings.TRS_MANIFEST_BOT_USER_NAME}'",
        ]

        default_xml = ET.parse(DEFAULT_XML_FILENAME)
        root = default_xml.getroot()
        project = root.findall("project")[0]
        bad_project = root.findall("project")[2]
        good_revisions = [(project, new_revision)]
        bad_revisions = [(bad_project, new_revision, settings.TUXSUITE_DOWNLOAD_URL)]

        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json.return_value = {}
        mock_response.text = "mock post response"
        mock_requests.post.return_value = mock_response

        failures = "## Failures \n" \
            "| project | current commit | new commit | logs |\n" \
            "|---------|----------------|------------|------|\n" \
            f"| {bad_project.attrib['name']} | {bad_project.attrib['revision']} | {new_revision} | [build.log]({settings.TUXSUITE_DOWNLOAD_URL}/build.log), [fetch.log]({settings.TUXSUITE_DOWNLOAD_URL}/fetch.log), [bitbake-environment]({settings.TUXSUITE_DOWNLOAD_URL}/bitbake-environment) |\n"

        expected_description = "This merge request was automatically generated by the TRS CI.\n\n" \
            f"{failures}\n\n" \
            "## Note \n" \
            "Each project with newer upstream changes has already been built individually via Tuxsuite.com.\n" \
            f"Please visit {settings.CI_PROJECT_URL}/-/jobs/{settings.CI_JOB_ID} for more details."

        self.assertFalse(create_manifest_mr(settings, good_revisions, bad_revisions, default_xml))
        mock_run_git.assert_has_calls([
            call(["checkout", "-b", expected_branch_name]),
            call(config_user + ["commit", "-m", f"{DEFAULT_XML_FILENAME}: tracking master", DEFAULT_XML_FILENAME]),
            call(["push", git_url]),
        ])
        mock_requests.post.assert_called_with(
            f"{settings.CI_API_V4_URL}/projects/{settings.CI_PROJECT_ID}/merge_requests",
            json={
                "id": settings.CI_PROJECT_ID,
                "source_branch": expected_branch_name,
                "target_branch": "main",
                "remove_source_branch": True,
                "title": f"Automatic updates on {DEFAULT_XML_FILENAME}",
                "allow_maintainer_to_push": True,
                "description": expected_description,
                "assignee_ids": assignees,
            },
            headers={
                "PRIVATE-TOKEN": settings.TRS_MANIFEST_BOT_USER_TOKEN,
                "Content-Type": "application/json",
            }
        )

    @patch("blueprints_ci.tip.requests")
    @patch("blueprints_ci.tip.run_git")
    def test_create_manifest_mr(self, mock_run_git, mock_requests):
        new_revision = "5"
        assignees = ["1", "2"]

        settings = self.sample_settings()
        settings.TRS_MANIFEST_MR_ASSIGNEE_IDS = ",".join(assignees)

        expected_branch_name = "track-master-" + datetime.now().strftime("%d-%m-%Y")
        git_user = f"project_{settings.CI_PROJECT_ID}_bot"
        git_url = f"https://{git_user}:{settings.TRS_MANIFEST_BOT_USER_TOKEN}@{settings.CI_SERVER_HOST}/{settings.CI_PROJECT_PATH}.git"
        config_user = [
            "-c", f"user.email='{git_user}@{settings.CI_SERVER_HOST}'",
            "-c", f"user.name='{settings.TRS_MANIFEST_BOT_USER_NAME}'",
        ]

        default_xml = ET.parse(DEFAULT_XML_FILENAME)
        root = default_xml.getroot()
        project = root.findall("project")[0]
        bad_project = root.findall("project")[2]
        good_revisions = [(project, new_revision)]
        bad_revisions = [(bad_project, new_revision, settings.TUXSUITE_DOWNLOAD_URL)]

        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json.return_value = {
            "iid": "1",
        }
        mock_requests.post.return_value = mock_response

        failures = "## Failures \n" \
            "| project | current commit | new commit | logs |\n" \
            "|---------|----------------|------------|------|\n" \
            f"| {bad_project.attrib['name']} | {bad_project.attrib['revision']} | {new_revision} | [build.log]({settings.TUXSUITE_DOWNLOAD_URL}/build.log), [fetch.log]({settings.TUXSUITE_DOWNLOAD_URL}/fetch.log), [bitbake-environment]({settings.TUXSUITE_DOWNLOAD_URL}/bitbake-environment) |\n"

        expected_description = "This merge request was automatically generated by the TRS CI.\n\n" \
            f"{failures}\n\n" \
            "## Note \n" \
            "Each project with newer upstream changes has already been built individually via Tuxsuite.com.\n" \
            f"Please visit {settings.CI_PROJECT_URL}/-/jobs/{settings.CI_JOB_ID} for more details."

        self.assertTrue(create_manifest_mr(settings, good_revisions, bad_revisions, default_xml))
        mock_run_git.assert_has_calls([
            call(["checkout", "-b", expected_branch_name]),
            call(config_user + ["commit", "-m", f"{DEFAULT_XML_FILENAME}: tracking master", DEFAULT_XML_FILENAME]),
            call(["push", git_url]),
        ])
        mock_requests.post.assert_called_with(
            f"{settings.CI_API_V4_URL}/projects/{settings.CI_PROJECT_ID}/merge_requests",
            json={
                "id": settings.CI_PROJECT_ID,
                "source_branch": expected_branch_name,
                "target_branch": "main",
                "remove_source_branch": True,
                "title": f"Automatic updates on {DEFAULT_XML_FILENAME}",
                "allow_maintainer_to_push": True,
                "description": expected_description,
                "assignee_ids": assignees,
            },
            headers={
                "PRIVATE-TOKEN": settings.TRS_MANIFEST_BOT_USER_TOKEN,
                "Content-Type": "application/json",
            }
        )
