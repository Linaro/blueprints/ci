Latest daily build:
<a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
    <img src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?passrate" />
</a>

<!--
  Gitlab allows certain HTML tags to be added as Markdown.
  The full list of allowed tags is here: https://github.com/gjtorikian/html-pipeline/blob/a363d620eba0076479faad86f0cf85a56b2b3c0a/lib/html/pipeline/sanitization_filter.rb
  Also note that there cannot be spaces between tags, otherwise HTML parser doesn't work
-->

<table>
  <thead>
    <th></th>
    <th>qemu <sup>TS</sup></th>
    <th>qemu-edk2 <sup>TS</sup></th>
    <th>rockpi4b <sup>TS</sup></th>
    <th>kv260 <sup>TS</sup></th>
    <th>zynqmp-zcu102 <sup>TS</sup></th>
  <thead>
  <tbody>
    <!-- build suite -->
    <tr>
      <th>build</th>
      <!-- qemu -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=qemu&suite=build">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=qemu&suite=build&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- qemu-edk2 -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=qemu-edk2&suite=build">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=qemu-edk2&suite=build&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rk3399-rock-pi-4b -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=rk3399-rock-pi-4b&suite=build">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=rk3399-rock-pi-4b&suite=build&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- zynqmp-kria-starter kv260 -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=kv260&suite=build">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=kv260&suite=build&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- zynqmp-zcu102 -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=zcu102&suite=build">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=zcu102&suite=build&passrate&title&hide_zeros=1" />
        </a>
    </tr>
    <!-- end of build suite -->
    <!-- boot suite (wait-is-system-running) -->
    <tr>
      <th>boot</th>
      <!-- qemu -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=qemu&suite=wait-is-system-running">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=qemu&suite=wait-is-system-running&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- qemu-edk2 -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=qemu-edk2&suite=wait-is-system-running">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=qemu-edk2&suite=wait-is-system-running&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rk3399-rock-pi-4b -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=rk3399-rock-pi-4b&suite=wait-is-system-running">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=rk3399-rock-pi-4b&suite=wait-is-system-running&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- kv260 -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=kv260&suite=wait-is-system-running">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=kv260&suite=wait-is-system-running&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- zynqmp-zcu102 -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests?environment=zcu102&suite=wait-is-system-running">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=zcu102&suite=wait-is-system-running&passrate&title&hide_zeros=1" />
        </a>
      </td>
    </tr>
    <!-- end of boot suite -->
  </tbody>
</table>


Legend:

* <img src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=donotexist&suite=donotexist&passrate&title&hide_zeros=1" /> : No results yet, tests are in progress

# ARM Blueprints CI Images

This CI is intended to build and test the following projects (links point to the latest successful build for each target):
- [![pipeline status](https://gitlab.com/Linaro/trustedsubstrate/meta-ts/badges/master/pipeline.svg)](https://gitlab.com/Linaro/trustedsubstrate/meta-ts/-/commits/master) meta-ts (Trusted Substrate): https://gitlab.com/Linaro/trustedsubstrate/meta-ts
  * Download images:
    * qemu
        * [flash.bin-qemu.gz](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/flash.bin-qemu.gz?job=build-meta-ts-qemuarm64-secureboot)
    * rockpi4b
        * [ts-firmware-rockpi4b.rootfs.wic.gz](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ts-firmware-rockpi4b.rootfs.wic.gz?job=build-meta-ts-rockpi4b)
    * zynqmp-kria-starter
        * [ImageA.bin.gz](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ImageA.bin.gz?job=build-meta-ts-zynqmp-kria-starter)
        * [ImageB.bin.gz](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ImageB.bin.gz?job=build-meta-ts-zynqmp-kria-starter)
    * zynqmp-zcu102
        * [ts-firmware-zynqmp-zcu102.rootfs.wic.gz](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ts-firmware-zynqmp-zcu102.rootfs.wic.gz?job=build-meta-ts-zynqmp-zcu102)

# Build strategy

There will be builds:
* Everyday at 5pm UTC (scheduled pipeline in https://gitlab.com/Linaro/blueprints/nightly-builds)
* On every:
  * push to the main branch (*main branch needs to be protected*)
  * new merge requests
  * new tags
* On these repositories
  * [![pipeline status](https://gitlab.com/Linaro/trustedsubstrate/meta-ts/badges/master/pipeline.svg)](https://gitlab.com/Linaro/trustedsubstrate/meta-ts/-/commits/master) https://gitlab.com/Linaro/trustedsubstrate/meta-ts

Git URLs for all components are described in [default.xml](https://gitlab.com/Linaro/trusted-reference-stack/trs-manifest/-/blob/main/default.xml).


## Pipelines

There are 3 types of events triggering pipelines:
* push to the main branch (*main branch needs to be protected*)
* new merge requests
* new tags

**NOTE:** No pipelines are triggered for non-main branches because merge requests originating from branches in the same repository would trigger 2 pipelines on the same change. Gitlab does not have a mechanism to prevent redundant pipelines.


### Push to main branch / new tags

Pipelines for these events take the git url and main branch (or new tag) of the component and change those in the manifest before triggering builds.


### Merge Requests

Because we are an open source project, running Gitlab's open source project, we are not allowed to have merge requests of forked repositories to trigger pipelines in parent projects.
For example, if Bob forks `meta-ts` and open a MR in `meta-ts` from Bob's fork, no pipeline is triggered. This is only available in [Gitlab's premium tier](https://docs.gitlab.com/ee/ci/pipelines/merge_request_pipelines.html#run-pipelines-in-the-parent-project). There is a hidden feature though: if Bob is a member in the parent project, Gitlab will run his pipeline on the parent project's resources.


# The builder: TuxSuite

All builds are actually run by a Linaro service called TuxSuite (tuxsuite.com). It is a newly created project initially developed to build Linux kernels at scale, abstracting all complexity related to disk space and parallel jobs. TuxSuite is in active development, and new features are added often. One of their latest added feature is the ability to re-use the same expertise learned in building kernels at scale to build OpenEmbedded projects.

One can visit their docs page (https://docs.tuxsuite.com) for further explanation of how it works. Below is an example of how we use TuxSuite to build Blueprints projects.


## How to build

Everything in TuxSuite happens in the cloud, so the only thing we need is a way to give it information about our build: that is called a "plan". Here is a working example of a plan:

```yaml
version: 1
name: "build-kas-meta-ts-qemuarm64"
description: "Plan to Build TrustedSubstrate for Qemu"
jobs:
- name: build-kas-meta-ts-qemuarm64
  bakes:
    - sources:
        kas:
          url: "https://gitlab.com/Linaro/trustedsubstrate/meta-ts.git"
          yaml: "ci/qemuarm64-secureboot.yml"
      name: meta-ts-secureboot
```

Save that into `plan.yml`, then run:

```bash
$ tuxsuite plan plan.yml
```

And wait for tuxsuite's magic:

```
Running Bake plan 'build-meta-ts-qemuarm64': 'Plan to Build TrustedSubstrate for Qemu'
Plan https://tuxapi.tuxsuite.com/v1/groups/blueprints/projects/trustedsubstrate/plans/2EAPthrX9oQnufe41igtEEm1dfV

uid: 2EAPthrX9oQnufe41igtEEm1dfV
⚙️  Provisioning: {'kas': {'url': 'https://gitlab.com/Linaro/trustedsubstrate/meta-ts.git', 'yaml': 'ci/qemuarm64-secureboot.yml'}} with None None for None: https://storage.tuxsuite.com/public/blueprints/trustedsubstrate/oebuilds/2EAPtsGSKBSa784LrKVSkFzOaWK/
🚀 Running: {'kas': {'url': 'https://gitlab.com/Linaro/trustedsubstrate/meta-ts.git', 'yaml': 'ci/qemuarm64-secureboot.yml'}} with None None for None: https://storage.tuxsuite.com/public/blueprints/trustedsubstrate/oebuilds/2EAPtsGSKBSa784LrKVSkFzOaWK/
🎉 Pass: {'kas': {'url': 'https://gitlab.com/Linaro/trustedsubstrate/meta-ts.git', 'yaml': 'ci/qemuarm64-secureboot.yml'}} with None None for None: https://storage.tuxsuite.com/public/blueprints/trustedsubstrate/oebuilds/2EAPtsGSKBSa784LrKVSkFzOaWK/

Summary: https://tuxapi.tuxsuite.com/v1/groups/blueprints/projects/trustedsubstrate/plans/2EAPthrX9oQnufe41igtEEm1dfV
2EAPtsGSKBSa784LrKVSkFzOaWK 🎉 Pass with container: ubuntu-20.04 machine: None targets: None
```

Yay! Now head to `https://storage.tuxsuite.com/<your-instance>` and browse through generated files.

**Note**: That actually built an whole OpenEmbedded image in the cloud, without you having to clean up your hard drive or restart the build due to connection issues. Because TuxSuite is hosted in AWS (Amazon Web Services), it is most certainly it will have a steady connection.

# How does it work?

This repository is meant to contain necessary CI scripts to be used by components elsewhere. Gitlab allows repositories point to CI scripts from external files. Therefore, there are three main files in this repository: `meta-ts.yml`, `meta-trs.yml` and `nightly-builds.yml`. Each of these are meant to be referenced within **Settings > CI/CD > General Pipelines** configuration in [linaro/trustedsubstrate/meta-ts.git](https://gitlab.com/Linaro/trustedsubstrate/meta-ts), [linaro/blueprints/ci.git](https://gitlab.com/Linaro/blueprints/ci/), and [linaro/blueprints/nightly-builds.git](https://gitlab.com/Linaro/blueprints/nightly-builds/), respectively.

```mermaid
graph TB
  sub1 --> linaro/trustedsubstrate/meta-ts
  sub4 --> linaro/blueprints/nightly-builds

  subgraph "linaro/blueprints/ci"
    sub1[meta-ts.yml]
    sub2[meta-trs.yml]
    sub4[nightly-builds.yml]

end
```

Then Gitlab will trigger new pipelines in each respective repository.
https://gitlab.com/Linaro/trustedsubstrate/ci/-/pipelines


## Subsystems integration

Here's an overview of how CI works along supporting subsystems (SQUAD, LAVA and TuxSuite):

```mermaid
sequenceDiagram
    participant user as Developer
    participant gitlab as Gitlab
    participant squad as SQUAD
    participant lava as LAVA
    participant tuxsuite as TuxSuite

    user->>+gitlab: git-push or MR
    gitlab->>tuxsuite: send build request
    tuxsuite-->>gitlab: build OK
    gitlab->>tuxsuite: download image
    tuxsuite-->>gitlab: image contents
    gitlab->>squad: submit build results
    squad-->>gitlab: build results submitted OK
    alt boot & test
        gitlab->>squad: submit test-job definition
        squad->>lava: submit test-job request
        lava->>gitlab: download OS and FIRMWARE
        gitlab-->>lava: OS and FIRMWARE contents
        lava-->>squad: test-job OK
	squad->>gitlab: trigger check job
        gitlab-->>squad: fetch test results
    end
```

It all starts with a `git-push` or a new merge request event. Gitlab parses the ci script for the repository and triggers a new pipeline. Gitlab then invokes `tuxsuite` cli which sends a build request to [TuxSuite](https://tuxsuite.com) service. After TuxSuite finishes building the requested image, Gitlab downloads it and store inside the job's artifact folder. Next step is to tell [SQUAD](https://qa-reports.linaro.org) that the build passed (or failed).

If there are target boards in [LAVA](https://ledge.validation.linaro.org), boot & test jobs are triggered. Gitlab generates a test-job definition, which is LAVA's specification on how to boot and test things, and use SQUAD to send it to LAVA. After LAVA downloads both OS and FIRMWARE from Gitlab, it triggers boot and tests that are specified in the test-job definition. LAVA then signals SQUAD that job is finished and results are finally fetched by SQUAD. When SQUAD finishes fetching all test results for a pipeline, it will trigger a job in Gitlab that will fetch test results from SQUAD, signaling developers that tests passed or failed.

The main reason why test-job definitions are sent to LAVA via SQUAD is so that SQUAD can collect results of all LAVA runs. And SQUAD does it nicely enough that allows users to be notified when tests fail or regress.


## How to test the BP CI changes locally

From a development standpoint, sometimes you want to test and debug changes locally before submitting a merge/pull request.

It requires to install `jinja2` and `squad-client` using `pip`.

Export `SQUAD_HOST` and `SQUAD_TOKEN` variables before run `./blueprints_ci_tests/run-self-tests.sh`


### Updating the docker image

By default, the CI docker image will be updated whenever there is a push to the Dockerfile in the main branch.
If no changes to Dockerfile are made but its internals require update, one can manually trigger a docker image
update by manually triggering a pipeline and setting a variable named `UPDATE_CI_DOCKER_IMAGE=1`.
