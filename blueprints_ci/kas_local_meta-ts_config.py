#!/usr/bin/env python3
import sys

sys.path.append(".")
from blueprints_ci import (  # noqa
    logger,
    Settings,
)


#
#   Build implementation
#
def kas_local_config(settings):
    """
        1. Check if it's running on a merge request in meta-ts
        2. Generate a kas-local-config.yml file changing the url for meta-ts repository
    """

    if not settings.IS_MERGE_REQUEST or settings.CI_PROJECT_NAME != "meta-ts":
        logger.warning("This Gitlab job is only supposed to run on merge-requests of meta-ts repository")
        return False

    if settings.CI_MERGE_REQUEST_SOURCE_BRANCH_NAME is None:
        logger.warning("The following environment variables are missing: ['CI_MERGE_REQUEST_SOURCE_BRANCH_NAME']")
        return False

    kas_local_config_contents = f"""header:
  version: 14

repos:
  meta-ts:
    url: https://gitlab.com/Linaro/trustedsubstrate/meta-ts.git
    branch: {settings.CI_MERGE_REQUEST_SOURCE_BRANCH_NAME}
    path: layers/meta-ts
"""

    logger.info("kas-local-config.yml:")
    logger.info(kas_local_config_contents)
    with open("kas-local-config.yml", "w") as fp:
        fp.write(kas_local_config_contents)

    return True


def main():
    settings = Settings()
    if settings.missing:
        return False

    return kas_local_config(settings)


if __name__ == "__main__":
    sys.exit(0 if main() else 1)
